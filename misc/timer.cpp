#include <iostream>
#include <iomanip>
#include <chrono>
#include <string>

class Timer
{
public:
    using Seconds = double;
    void start()
    {
        m_start = std::chrono::high_resolution_clock::now();
    }
    void stop()
    {
        m_stop = std::chrono::high_resolution_clock::now();
    }
    Seconds duration()
    {
        std::chrono::duration<Seconds> dur(m_stop - m_start);
        return dur.count();
    }
public:
    using Timepoint = decltype(std::chrono::high_resolution_clock::now());
    Timepoint m_start;
    Timepoint m_stop;
};

class ScopeTimer
{
public:
    ScopeTimer(const std::string& s) : m_msg{s}
    {
        m_timer.start();
    }
    ~ScopeTimer()
    {
        m_timer.stop();
        std::cout << "Timer \"" << m_msg << "\": " << m_timer.duration() << "s\n";
    }
private:
    Timer m_timer;
    std::string m_msg;
};

constexpr int len = 129;

void oneOperation(double arr[len][len][len][len])
{
    std::cout << "One operation." << std::endl;
    ScopeTimer t("One operation");
    double sum{0};
    for (int i = 0; i < len; ++i)
    {
        for (int j = 0; j < len; ++j)
        {
            for (int k = 0; k < len; ++k)
            {
                for (int l = 0; l < len; ++l)
                {
                    sum += arr[l][k][j][i];
                }
            }
        }
    }
    std::cout << "Random result: " << sum << std::endl;
}

void twoOperations(double arr[len][len][len][len])
{
    std::cout << "Two operations." << std::endl;
    ScopeTimer t("Two operations");
    double sum{0};
    double halfSum{0};
    for (int i = 0; i < len; ++i)
    {
        for (int j = 0; j < len; ++j)
        {
            for (int k = 0; k < len; ++k)
            {
                for (int l = 0; l < len; ++l)
                {
                    sum += arr[i][j][k][l];
                    halfSum += 0.5 * arr[i][j][k][l];
                }
            }
        }
    }
    std::cout << "Random result: " << sum << ", " << halfSum << std::endl;
}
#include <cmath>
int main()
{
    {
        ScopeTimer sc("Klausul");
        float f = 0.f;
        volatile float k = 2.f;
        for (int i = 0; i < 1000000000; ++i)
        {
            //float kk = k * k;
            //f = 1200.f - 3.14159265f * (kk + k * std::sqrt(kk + 400));
            k = k - f + f;
        }
    }
    auto arr = new double[len][len][len][len];
    for (int i = 0; i < len; ++i)
    {
        for (int j = 0; j < len; ++j)
        {
            for (int k = 0; k < len; ++k)
            {
                for (int l = 0; l < len; ++l)
                {
                    arr[i][j][k][l] = 0.7;
                }
            }
        }
    }
    oneOperation(arr);
    twoOperations(arr);
}

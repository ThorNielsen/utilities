#include <iostream>
#include <sstream>
#include <iomanip>

#include <chrono>
#include <thread>

#define UNITTEST(x) if (!(x))\
{\
    std::cerr << "Test at " __FILE__ ":" << __LINE__ << " failed." << std::endl;\
}

std::string progressbar(float progress, size_t maxlen,
                        std::string before = "", std::string after = "",
                        bool use_colours = false)
{
    constexpr const char* progress_symbol[] =
    {
        " ", "▏", "▎", "▍", "▌", "▋", "▊", "▉", "█"
    };

    constexpr const char* colour[] =
    {
        "\033[31m", // red
        "\033[33m", // yellow
        "\033[32m"  // green
    };
    size_t percentage = progress * 100.f + 0.5f;
    if (maxlen >= 9) maxlen -= 9;
    if (before.size() && maxlen >= 2)
    {
        maxlen -= (maxlen < before.size() + 2) ? maxlen : before.size() + 2;
    }
    if (after.size()  && maxlen >= 1)
    {
        maxlen -= (maxlen < after.size() + 1) ? maxlen : after.size() + 1;
    }
    if (!maxlen) ++maxlen;
    size_t len = progress * 8 * maxlen;
    size_t filled = len / 8;

    std::ostringstream s;
    if (before.size())
    {
        s << before << ": ";
    }
    s << "[";
    if (use_colours)
    {
        s << colour[std::min((int)(progress*3.0f), 3)];
    }
    for (size_t i = 0; i < filled; ++i)
    {
        s << progress_symbol[8];
    }
    s << ((percentage == 100) ? progress_symbol[8] : progress_symbol[len%8]);
    for (size_t i = filled+1; i < maxlen; ++i)
    {
        s << " ";
    }
    if (use_colours)
    {
        s << "\033[0m";
    }
    s << "] (" << std::setw(3) << percentage << "%)";
    if (after.size())
    {
        s << " " << after;
    }
    return s.str();
}

int main()
{
    int len;
    std::cin >> len;
    len = std::max(len, 0);
    for (float f = 0.f; f <= 1.01f; f += 0.0001)
    {
        std::cout << "\r" << progressbar(f, len, "Eating HDD", "", true) << std::flush;
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }
    std::cout << "\n";
}

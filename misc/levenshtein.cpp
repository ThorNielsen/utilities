#include <iostream>
#include <string>
#include <vector>

int ldist(const std::string& a,
         const std::string& b,
         std::vector<std::vector<int>>& mem,
         int i,
         int j)
{
    if (!i || !j) return std::max(i, j);
    mem[i][j] = std::min(ldist(a, b, mem, i-1, j-1) + 1*(i == j),
                   std::min(ldist(a, b, mem, i, j-1) + 1,
                            ldist(a, b, mem, i-1, j) + 1));
    if (i == a.size() || j == b.size())
    if (a[i] == b[j]) return ldist(a, b, mem, i+1, j+1);

}

int ldist(const std::string& a, const std::string& b)
{
    std::vector<std::vector<int>> mem(a.size(), std::vector<int>(b.size(), -1));
    return ldist(a, b, mem, a.size()-1, b.size() - 1);
}

int main()
{
    std::cout << ldist("kitten", "sitten");
}

#!/bin/bash
cp build/filetoarray ~/bin/filetoarray
cp build/argparse ~/bin/argparse
cp build/amalgamate-images ~/bin/amalgamate-images
cp build/json-pretty ~/bin/json-pretty

#include <fstream>
#include <iomanip>
#include <iostream>
#include <string>
#include <vector>

using U8 = unsigned char;

std::vector<U8> readFile(std::istream& file)
{
    file.seekg(0, std::ios::end);
    auto bytes = file.tellg();
    file.seekg(0);
    std::vector<U8> contents(bytes);
    file.read(reinterpret_cast<char*>(contents.data()), bytes);
    return contents;
}

size_t stringToInt(std::string str, size_t fail)
{
    std::stringstream ss(str);
    size_t s;
    ss >> s;
    if (ss.good()) return s;
    return fail;
}

int main(int argc, char* argv[])
{
    std::string filename;
    size_t indent = 4;
    size_t maxWidth = 80;
    if (argc > 1)
    {
        filename = argv[1];
    }
    if (argc > 2)
    {
        indent = stringToInt(argv[2], indent);
    }
    if (argc > 3)
    {
        maxWidth = stringToInt(argv[3], maxWidth);
    }
    std::ifstream file(filename);
    std::istream* readFrom = &file;
    if (!file.is_open()) readFrom = &std::cin;
    auto data = readFile(*readFrom);
    std::string pad;
    while (pad.size() < indent)
        pad.push_back(' ');
    std::string longpad = pad + "    ";
    std::cout << pad << "const U8 data[] =\n";
    std::cout << pad << "{\n";
    const U8* at = data.data();
    const U8* end = data.data() + data.size();
    size_t outputCnt = 0;
    std::cout << std::hex << std::setfill('0');
    while (at != end)
    {
        if (!outputCnt)
        {
            std::cout << longpad;
            outputCnt += longpad.size();
        }
        std::cout << "0x" << std::setw(2) << (int)*at << ',';
        outputCnt += 6;
        if (outputCnt + 5 > maxWidth)
        {
            outputCnt = 0;
            std::cout << '\n';
        }
        else
        {
            std::cout << ' ';
        }
        ++at;
    }
    if (outputCnt) std::cout << '\n';
    std::cout << pad << "};\n";
}

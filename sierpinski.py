# Copyright (c) 2017 Thor G. Nielsen

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

def sierpinskiTriangle(n):
    if n <= 0: return "x_x^x"
    s = "{" + sierpinskiTriangle(n-1) + "}"
    return s + "_" + s + "^" + s

def sierpinskiCarpet(n):
    #if n <= 0: return "x"
    if n <= 0: return "\cellcolor[HTML]{000000}"
    sc = sierpinskiCarpet(n-1)
    sc3 = sc + "&" + sc + "&" + sc
    sc2 = sc + "&&" + sc
    s = "\\begin{array}{ccc}" + sc3 + "\\\\" + sc2 + "\\\\" + sc3 + "\\\\&&\\\\\\end{array}\n"
    return s

fname = input("File name (blank for STDOUT): ")

fractal = sierpinskiTriangle(int(input("Number of iterations (triangle): ")))
fractal2 = sierpinskiCarpet(int(input("Number of iterations (carpet): ")))

s = ""
s = s +"""\\documentclass[11pt]{article}
\\usepackage[utf8]{inputenc}
\\usepackage[margin=25mm,paperwidth=70cm,paperheight=70cm]{geometry}
\\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\\begin{document}
\\["""
s = s + fractal + "\\]\n\\[\n" + fractal2 + "\\]\n\\end{document}"

if fname == "": print(s)
else:
    try:
        open(fname, "w+").write(s)
    except:
        print("Could not write to file.")


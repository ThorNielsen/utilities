# Copyright (c) 2017 Thor G. Nielsen

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from math import *

def sign(x):
    if x > 0: return 1
    elif x < 0: return -1
    else: return 0

def minabs(x, y):
    if abs(x) < abs(y): return x
    else: return y

def maxabs(x, y):
    if abs(x) > abs(y): return y
    else: return x

def findroot_bisect(func, a, b, maxErr = 1e-8, minInterval = 1e-12, maxSteps = 256):
    aVal = func(a)
    bVal = func(b)
    iWidth = b - a
    if sign(aVal) == sign(bVal): return (False, 0)
    cVal = 0
    while maxSteps > 0:
        iWidth = iWidth / 2
        c = a + iWidth
        cVal = func(c)
        if abs(iWidth) < minInterval or abs(cVal) < maxErr: return (True, c)
        if sign(aVal) != sign(cVal):
            bVal = cVal
            b = c
        else:
            aVal = cVal
            a = c
        maxSteps = maxSteps - 1
    return (False, cVal) # Conditions not fulfilled, but value might be close
                         # enough to the real result.

def findroot_secant(func, a, b, maxErr = 1e-8, minInterval = 1e-12, maxSteps = 48):
    aVal = func(a)
    bVal = func(b)
    if aVal == bVal and a != b:
        b = a + (b-a) / 2
        bVal = func(b)
    while maxSteps > 0:
        if abs(aVal) > abs(bVal):
            (a, b, aVal, bVal) = (b, a, bVal, aVal)
        if aVal == bVal: return (False, 0)
        s = (b - a)/(bVal - aVal)
        (b, bVal) = (a, aVal)
        a = a - s * aVal
        aVal = func(a)
        if abs(aVal) < maxErr or abs(b - a) < minInterval:
            return (True, a)
        maxSteps = maxSteps - 1
    return (False, aVal)

fun = eval("lambda x:" + input("Enter function in x: "))

print("Secant method.")
(a, b) = map(lambda v:float(eval(v)), input("Enter starting points: ").split())
(zeroExists, val) = findroot_secant(fun, a, b, 1e-15, 1e-20)
if zeroExists: print("Zero found at x = " + str(val))
else: print("No zero found.")

print("Bisection method.")
(a, b) = map(lambda v:float(eval(v)), input("Enter search interval: ").split())
(zeroExists, val) = findroot_bisect(fun, a, b, 1e-15, 1e-20)
if zeroExists: print("Zero found at x = " + str(val))
else: print("No zero found.")


# Copyright (c) 2017 Thor G. Nielsen

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import numpy as np
from fractions import Fraction as Frac

def typesetFrac(f):
    if f.denominator == 1:
        return str(f.numerator)
    elif f.numerator < 0:
        return "-\\frac{"+str(abs(f.numerator))+"}{"+str(f.denominator) + "}"
    else:
        return "\\frac{" + str(f.numerator) + "}{" + str(f.denominator) + "}"

showcol = -1

def typesetMatrix(mat : np.array):
    s = "\\begin{pmatrix}"
    _, cols = mat.shape
    if 0 <= showcol <= cols:
        s += "["
        for i in range(cols):
            if showcol == i: s += "|"
            s += "c"
        if showcol == cols: s += "|"
        s += "]"
    s += "\n"
    h, w = mat.shape
    for i in range(h):
        for j in range(w):
            s += typesetFrac(mat[i, j])
            if j + 1 < w:
                s += "&"
        s += "\\\\\n"
    s += "\\end{pmatrix}&\n"
    return s

def addRows(rows, coeffs, addends):
    s = "\\begin{matrix*}[r]\n"
    for r in rows:
        if coeffs[r] != Frac(0, 1):
            scalar = typesetFrac(coeffs[r])
            if coeffs[r] > Frac(0, 1):
                scalar = "+" + scalar
            if coeffs[r] == Frac(1, 1):
                scalar = "+"
            if coeffs[r] == Frac(-1, 1):
                scalar = "-"
            s += scalar + "R_{" + str(addends[r]+1) + "}"
        s += "\\\\\n"
    return s + "\\end{matrix*}\\\\\n"

def exchangeRows(a, b):
    if a > b: a, b = b, a
    return "R_{" + str(a+1) + "}\\leftrightarrow R_{" + str(b+1) + "}\\\\\n"

def mulRows(rows, coeffs):
    s = "\\begin{matrix*}[r]\n"
    for r in rows:
        if coeffs[r] != Frac(0, 1):
            scalar = typesetFrac(coeffs[r])
            if coeffs[r] == Frac(1, 1):
                scalar = ""
            if coeffs[r] == Frac(-1, 1):
                scalar = "-"
            s += scalar + "\\cdot R_{" + str(r+1) + "}"
        s += "\\\\\n"
    return s + "\\end{matrix*}\\\\\n"

def typesetOp(op, a, b, c = Frac(0, 1)):
    c = typesetFrac(c)
    if op == "T":
        if a > b: a, b = b, a
        if a < 10 and b < 10:
            return "T_{" + str(a) + str(b) + "}"
        return "T_{" + str(a) + "," + str(b) + "}"
    elif op == "S":
        if a < 10 and b < 10:
            return "S_{" + str(a) + str(b) + "}(" + c + ")"
        return "S_{" + str(a) + "," + str(b) + "}(" + c + ")"
    elif op == "M":
        return "M_{" + str(a) + "}(" + c + ")"
    else:
        raise Exception("Not implemented!")

def gauss(mat : np.array, fullyReduce = True):
    h, w = mat.shape
    m = mat.copy()
    s = "\\begin{align}\n" + typesetMatrix(m) + "\\\\\n"
    rows = [i for i in range(h)]
    cRow = 0
    for j in range(w):
        maxVal, maxIdx = Frac(0, 1), -1
        for i in range(cRow, h):
            if abs(m[i, j]) > maxVal:
                maxVal, maxIdx = abs(m[i, j]), i
        if maxVal == Frac(0, 1):
            continue # Another later column might have a non-zero entry
        if m[cRow, j] == Frac(1, 1):
            maxVal, maxIdx = Frac(1, 1), cRow
        if maxIdx != cRow:
            tmp = m[maxIdx].copy()
            m[maxIdx] = m[cRow]
            m[cRow] = tmp
            s += typesetMatrix(m)
            s += exchangeRows(cRow, maxIdx)

        if fullyReduce:
            scale = False
            if m[cRow, j] != Frac(1, 1):
                scale = True
                coeffs = [Frac(0, 1) for _ in rows]
                coeffs[cRow] = Frac(1, 1) / m[cRow, j]
            m[cRow] /= Frac(1, 1) * m[cRow, j] # Prevents degeneration to integers
            if scale:
                s += typesetMatrix(m)
                s += mulRows(rows, coeffs)

        addends = [i for i in range(h)]
        coeffs = [Frac(0, 1) for _ in rows]
        for r in range(h):
            if r == cRow: continue
            coeffs[r], addends[r] = Frac(-1, 1) * m[r, j] / m[cRow, j], cRow
            m[r] -= m[cRow] * m[r,j] / m[cRow, j]
        if coeffs != [Frac(0, 1) for _ in rows]:
            s += typesetMatrix(m)
            s += addRows(rows, coeffs, addends)
        cRow += 1
    return s + "\\end{align}\n"

msg = """% --------------- Add this to your LaTeX preamble ---------------
\\usepackage{amsmath}
\\usepackage{mathtools}

\\makeatletter
\\renewcommand*\\env@matrix[1][*\\c@MaxMatrixCols c]
{
\\hskip-\\arraycolsep
\\let\\@ifnextchar\\new@ifnextchar
\\array{#1}}
\\makeatother

\\renewcommand*{\\arraystretch}{1.25}
% --------------- Add this to your LaTeX preamble ---------------
"""

print(msg)

print("Enter matrix (fractions are entered using Frac(n, d)): ")
mat = input()
lst = eval(mat)
arry = np.array(lst)
arry = arry.astype('object')
for i in range(len(arry)):
    for j in range(len(arry[i])):
        arry[i][j] = Frac(arry[i][j], 1)

showcol = int(input("Separator? "))

def truthy(x):
    return x.lower() in ["ja", "j", "yes", "y"]

full = truthy(input("Reduce fully? "))

print(gauss(arry, full))

#include <iostream>
#include <fstream>
#include <kraken/utility/json.hpp>

int main(int argc, char* argv[])
{
    std::ifstream inputFile;
    if (argc > 1)
    {
        inputFile.open(argv[1]);
        if (!inputFile.is_open())
        {
            std::cerr << "Couldn't open \"" << argv[1] << "\" for reading.\n";
            return 1;
        }
    }
    std::istream& input = inputFile.is_open() ? inputFile : std::cin;
    kraken::JSONValue json;
    input >> json;
    kraken::prettyPrint(std::cout, json);
    return 0;
}

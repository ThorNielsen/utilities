#!/bin/bash
if [[ -d build ]]; then
    rm -r build
fi
mkdir build
g++ filetoarray.cpp -O3 -s -o build/filetoarray
g++ argparse.cpp -O3 -s -o build/argparse
g++ amalgamate-images.cpp -O3 -s -lkraken-image -o build/amalgamate-images
g++ json-pretty.cpp -O3 -s -lkraken-utility -o build/json-pretty

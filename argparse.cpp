#include <iostream>
#include <map>
#include <stdexcept>
#include <string>

std::string escape(std::string source)
{
    std::string ret;
    for (size_t i = 0; i < source.size(); ++i)
    {
        if (source[i] == '\'')
        {
            ret.push_back('\\');
            ret.push_back('\'');
        }
        else if (source[i] == '\\')
        {
            ret.push_back('\\');
            ret.push_back('\\');
        }
        else
        {
            ret.push_back(source[i]);
        }
    }
    return ret;
}

int main(int argc, char* argv[])
{
    if (argc < 3)
    {
        std::cerr << "Too few arguments passed.\n";
        return 1;
    }
    std::map<std::string, std::string> parsedArgs;
    std::string endOfArgs = argv[1];
    int currArg = 2;
    for (; currArg < argc; ++currArg)
    {
        std::string arg = argv[currArg];
        if (arg == endOfArgs) break;
        if (currArg+1 >= argc)
        {
            std::cerr << "Argument '" << arg << "' missing its default value.\n";
            return 2;
        }
        std::string value = argv[currArg+1];
        parsedArgs[arg] = value;
        ++currArg;
    }
    if (currArg >= argc)
    {
        std::cerr << "End of argument list not found.\n";
        return 3;
    }
    ++currArg;
    for (; currArg < argc; ++currArg)
    {
        std::string arg = argv[currArg];
        if (parsedArgs.find(arg) == parsedArgs.end())
        {
            std::cerr << "Argument '" << arg << "' not recognised.\n";
            return 4;
        }
        if (currArg+1 >= argc)
        {
            std::cerr << "Argument '" << arg << "' missing its value.\n";
            return 5;
        }
        parsedArgs[arg] = std::string(argv[currArg+1]);
        ++currArg;
    }
    for (auto argpair : parsedArgs)
    {
        std::cout << argpair.first << "='" << escape(argpair.second) << "' \n";
    }
}

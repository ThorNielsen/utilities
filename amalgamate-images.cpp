#include <fstream>
#include <iostream>
#include <kraken/image/image.hpp>

using namespace kraken;
using namespace image;

bool readImage(std::string filename, Image& imgOut)
{
    std::ifstream input(filename);
    if (!input.is_open())
    {
        std::cerr << "Failed to open image \"" << filename << "\".\n";
        return false;
    }
    return imgOut.read(input);
}

int main(int argc, char* argv[])
{
    if (argc != 6)
    {
        std::cerr << "Usage: amalgamate-images [image] [image-aux] [alpha] [alpha-aux] [output]\n";
        return 1;
    }

    Image img, imgAux, alpha, alphaAux, output;
    if (!readImage(argv[1], img)) return 2;
    if (!readImage(argv[2], imgAux)) return 2;
    if (!readImage(argv[3], alpha)) return 2;
    if (!readImage(argv[4], alphaAux)) return 2;

    if (img.colourType() != imgAux.colourType()
        || img.width() != imgAux.width()
        || img.height() != imgAux.height())
    {
        std::cerr << "Error: Image and auxiliary image must have same properties (bit depth, channels and dimensions).\n";
        return 3;
    }

    if (alpha.colourType() != alphaAux.colourType()
        || alpha.width() != alphaAux.width()
        || alpha.height() != alphaAux.height())
    {
        std::cerr << "Error: Alpha and auxiliary alpha must have same properties (bit depth, channels and dimensions).\n";
        return 3;
    }

    if (img.width() != alpha.width() || img.height() != alpha.height())
    {
        std::cerr << "Error: All input images must have same dimensions.\n";
        return 4;
    }

    output.resize(img.width(), img.height(), img.colourType());

    output.fill([&](U32 x, U32 y)
    {
        return alpha.getPixel(x, y) >= alphaAux.getPixel(x, y)
            ? img.getPixel(x, y)
            : imgAux.getPixel(x, y);
    });

    std::ofstream fileOut(argv[5]);
    if (!fileOut.is_open())
    {
        std::cerr << "Failed to open \"" << argv[5] << "\" for writing.\n";
        return 5;
    }
    if (!output.write(fileOut, "png"))
    {
        std::cerr << "Failed to write image to \"" << argv[5] << "\".\n";
        return 6;
    }
    return 0;
}

